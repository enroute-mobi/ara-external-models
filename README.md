# ara-external-models

Protocol Buffers structures for exchanges with [Ara](https://bitbucket.org/enroute-mobi/ara)

## Modifications

To create or change the models, you need Protocol Buffers. [Checkout this link](https://developers.google.com/protocol-buffers/docs/gotutorial#compiling-your-protocol-buffers)

Change the protobuf files in /inputs, then run `make`