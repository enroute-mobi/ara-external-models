module bitbucket.org/enroute-mobi/ara-external-models

go 1.12

require (
	github.com/golang/protobuf v1.5.2
	google.golang.org/protobuf v1.27.1 // indirect
)
